# Oil Dispatch management

Creative Energies Total Dispatch is the ONLY dispatch and billing system built specifically for petroleum carriers.  With over 25 years of integrated knowledge and enhancements, 

Established in 1989 by Howard Ehrlich

## Dispatch management provides the following features:
- Effortless and accurate order entry
- Straightforward and easily understood traditional petroleum dispatch board.
- Customized, flexible, easy to use billing platform including: automatic calculations for split loads, two terminal loads, rate - tables, minimum gallons add on charges, insurance charges, date driven surcharges...and more.
- Our CE CONNECT! Mobile app links drivers and dispatchers.  Collecting loading times, terminal BOLs and even document imaging.
- Web based portal for your customers to view, search and download Bill of Ladings, Invoices, and load statuses in real-time.
- Quick Up and Running, easy and no additional development or implementation staff is needed
- Cloud based, very low investment IT architecture.  

# Technologies used
- Java 11, Java 8, Spring, Spring Boot, Microservices, Angular 4, 7, 9, 11, Python
- Android
- Amazon Cloud (AWS)
- Google Cloud (GCP)
- Kotlin
- PWA, React js, React Native
- MySql, Sql Lite, 
- Speech Recognition, Speech to Text, Text to Speech
- Google Maps, Geo Fencing, Path Tracing
- Prediction and load forcasting
- Image processing, Image compression, FTP, Email processing
- IOT sensors
- Cloud monitoring
- AWS
- Apache Kafka
- Angular 9
- Android

# Description
The platform developed is a multi tenant Billing and dispatch management system

# Disclaimer
The code base is a private repo and client doesn't want the codebase to be disclosed,
I can explain the code base and flow of the app by screenshare or have a code review session
for some portions of the code

# Images

![](https://rodionsolutions.com/assets/images/creative-energies/dispatchmanagement.PNG)
![](https://rodionsolutions.com/assets/images/creative-energies/1.PNG)
![](https://rodionsolutions.com/assets/images/creative-energies/2.PNG)
![](https://rodionsolutions.com/assets/images/creative-energies/3.PNG)
![](https://rodionsolutions.com/assets/images/creative-energies/Picture1.png)
